import React, { Fragment } from "react";
import "./Test.scss";
import { CardCarItem } from "../../assets/components/CardCarItem/CardCarItem";

export const Test = () => {
  return (
    <Fragment>
      <div className="test d-flex flex-wrap gap-5 justify-content-center">
        <CardCarItem
          name="Avanza"
          type="CUV"
          price={430000}
          desc="The Toyota Avanza and Daihatsu Xenia are multi-purpose vehicles (MPV). The Toyota Avanza and Daihatsu Xenia are multi-purpose vehicles (MPV)."
          seat={4}
          gear="Manual"
          year={2018}
        />
        <CardCarItem
          name="Avanza"
          type="CUV"
          price={430000}
          desc="The Toyota Avanza and Daihatsu Xenia are multi-purpose vehicles (MPV)."
          seat={4}
          gear="Manual"
          year={2018}
        />
        <CardCarItem
          name="Avanza"
          type="CUV"
          price={430000}
          desc="The Toyota Avanza and Daihatsu Xenia are multi-purpose vehicles (MPV)."
          seat={4}
          gear="Manual"
          year={2018}
        />
        <CardCarItem
          name="Avanza"
          type="CUV"
          price={430000}
          desc="The Toyota Avanza and Daihatsu Xenia are multi-purpose vehicles (MPV)."
          seat={4}
          gear="Manual"
          year={2018}
        />
        <CardCarItem
          name="Avanza"
          type="CUV"
          price={430000}
          desc="The Toyota Avanza and Daihatsu Xenia are multi-purpose vehicles (MPV)."
          seat={4}
          gear="Manual"
          year={2018}
        />
        <CardCarItem
          name="Avanza"
          type="CUV"
          price={430000}
          desc="The Toyota Avanza and Daihatsu Xenia are multi-purpose vehicles (MPV)."
          seat={4}
          gear="Manual"
          year={2018}
        />
      </div>
    </Fragment>
  );
};
